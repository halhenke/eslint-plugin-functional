/**
 * @fileoverview Tests for no-mutate-object rule.
 * @author Hal Henke
 * @copyright 2015 Hal Henke. All rights reserved.
 */

"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var rule = require("../../../lib/rules/no-mutate-object"),
    RuleTester = require("eslint").RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

var ruleTester = new RuleTester();
ruleTester.run("no-mutate-object", rule, {
    valid: [
        "var guy = foo.bar",
        // {
        //   code: "const x = 0;",
        //   parserOptions: { ecmaVersion: 6 }
        // },
        // {
        //   code: "var {dob, fob} = bob();",
        //   parserOptions: { ecmaVersion: 6 }
        // }
    ],
    invalid: [
        {
          code: "foo.bar = 45",
          errors: [{ message: "Attempt to mutate object 'foo': Setting properties on objects is forbidden", type: "MemberExpression"}]
        },
        {
          code: "foo.bar += 45",
          errors: [{ message: "Attempt to mutate object 'foo': Setting properties on objects is forbidden", type: "MemberExpression"}]
        },
        {
          code: "Array.prototype.bar = 45",
          errors: [{ message: "Attempt to mutate object 'undefined': Setting properties on objects is forbidden", type: "MemberExpression"}]
        }
    ]
});
