# eslint-plugin-2

A set of rules to facilitate functional programming in Javascript

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-2`:

```
$ npm install eslint-plugin-2 --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-2` globally.

## Usage

Add `2` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "2"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "2/rule-name": 2
    }
}
```

## Supported Rules

* Fill in provided rules here





