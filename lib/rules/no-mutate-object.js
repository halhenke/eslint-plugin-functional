/** * @fileoverview A rule to preven mutation of existing objects
 * @author Hal Henke
 * @copyright 2016 Hal Henke. All rights reserved.
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = function(context) {
    /**
     * Adds multiple items to the tail of an array.
     * @param {any[]} array - A destination to add.
     * @param {any[]} values - Items to be added.
     * @returns {void}
     */
    var pushAll = Function.apply.bind(Array.prototype.push);

    return {
        "MemberExpression": function(node) {
          if (node.parent && node.parent.type === 'AssignmentExpression') {
            context.report({
              node: node,
              message: "Attempt to mutate object '{{ obj }}': Setting properties on objects is forbidden",
              data: {
                obj: node.object.name
              }
            });
          }
        }
    };

};

module.exports.schema = [];
